CREATE TABLE IF NOT EXISTS friends(
  user_guid bigint,
  friend_guid bigint,
  timestamp timestamp,
  PRIMARY KEY(user_guid, friend_guid)
) ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS nostr_events (
    id varchar(64),
    pubkey varchar(64),
    created_at timestamp,
    kind int,
    tags text,
    e_ref varchar(64),
    p_ref varchar(64),
    content text,
    sig varchar(128),
    PRIMARY KEY (id, pubkey)
) ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS nostr_users (
    pubkey varchar(64),
    user_guid bigint,
    is_external boolean,
    PRIMARY KEY (pubkey)
) ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS nostr_kind_1_to_activity_guid (
    id varchar(64),
    activity_guid bigint,
    owner_guid bigint,
    is_external boolean,
    PRIMARY KEY (id, activity_guid)
) ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS nostr_pubkey_whitelist (
    pubkey varchar(64),
    PRIMARY KEY (pubkey)
) ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS clustered_recs(
  cluster_id int,
  entity_guid bigint,
  entity_owner_guid bigint,
  score float(5,2),
  first_engaged timestamp NULL DEFAULT NULL,
  last_engaged timestamp NULL DEFAULT NULL,
  last_updated timestamp NULL DEFAULT NULL,
  time_created timestamp DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  total_views bigint,
  total_engagement bigint,
  PRIMARY KEY(cluster_id, entity_guid),
  INDEX (score)
) ENGINE=InnoDB;
